package ge.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var oper: String = ""
    var result: Double = 0.0
    var value1: Double = 0.0
    var value2: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
        button0.setOnClickListener(this)
    }

    fun dot(view: View){
        if(result_text.text.isEmpty()){
            result_text.text = result_text.text.toString() + "0" + "."
            
        }
        else{
            result_text.text = result_text.text.toString() + "."
        }
        buttondot.isClickable = false
    }

    fun equals(view: View){
        if(result_text.text.isNotEmpty()){
            value2 = result_text.text.toString().toDouble()

            checkoper(value1 , value2)
        }
    }

    fun divide(view: View){
        if(result_text.text.isNotEmpty()) {
            oper = ":"
            value1 = result_text.text.toString().toDouble()
            result_text.text = ""
        }
    }

    fun multi(view: View){
        if(result_text.text.isNotEmpty()) {
            oper = "*"
            value1 = result_text.text.toString().toDouble()
            result_text.text = ""
        }
    }

    fun minus(view: View){
        if(result_text.text.isNotEmpty()) {
            oper = "-"
            value1 = result_text.text.toString().toDouble()
            result_text.text = ""
        }
    }

    fun plus(view: View){
        if(result_text.text.isNotEmpty()) {
            oper = "+"
            value1 = result_text.text.toString().toDouble()
            result_text.text = ""
        }
    }

    fun delete(view: View) {
        if(result_text.text == "Error"){
            result_text.text = ""
        }

        if(result_text.text.isNotEmpty()){
            result_text.text = result_text.text.substring(0 , result_text.text.length-1)
        }

    }

    override fun onClick(v: View?) {
        val button = v as Button
        result_text.text = result_text.text.toString() + button.text.toString()
    }


    fun checkoper(value1 : Double, value2: Double){
        if(oper == "+"){
            result = value1 + value2
            result_text.text = result.toString()
        }

        if(oper == "-"){
            result = value1 - value2
            result_text.text = result.toString()
        }

        if(oper == "*"){
            result = value1 * value2
            result_text.text = result.toString()
        }

        if(oper == ":"){
            if(value2 == 0.0){
                result_text.text = "Error"
            }
            else{
                result = value1 / value2
                result_text.text = result.toString()
            }
        }
    }


}